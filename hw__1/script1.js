function Hamburger(size, stuffing) {
    try {
        if (!size) {
            HamburgerException('No given size');
        }
        if (!stuffing) {
            HamburgerException('No given stuffing');
        }
        if (size !== Hamburger.SIZE_LARGE && size !== Hamburger.SIZE_SMALL) {
            HamburgerException(`invalid size ${size}`);
        }
        if (stuffing !== Hamburger.STUFFING_CHEESE && stuffing !== Hamburger.STUFFING_POTATO &&
            stuffing !== Hamburger.STUFFING_SALAD) {
            HamburgerException(`invalid stuffing ${stuffing}`);
        }

        this.size = size;
        this.stuffing = stuffing;
        this.toppings = [];

    } catch (error) {
        console.log(`${error.name}: ${error.message}`);
    }
}

Hamburger.SIZE_SMALL = 'SIZE_SMALL';
Hamburger.SIZE_LARGE = 'SIZE_LARGE';
Hamburger.STUFFING_CHEESE = 'STUFFING_CHEESE';
Hamburger.STUFFING_SALAD = 'STUFFING_SALAD';
Hamburger.STUFFING_POTATO = 'STUFFING_POTATO';
Hamburger.TOPPING_MAYO = 'TOPPING_MAYO';
Hamburger.TOPPING_SPICE = 'TOPPING_SPICE';

Hamburger.prototype.addTopping = function (topping) {
    try {
        this.toppings.forEach(item => {
            if (topping === item) {
                HamburgerException(`Duplicate topping ${topping}`)
            }
        });
        if (topping !== Hamburger.TOPPING_MAYO && topping !== Hamburger.TOPPING_SPICE) {
            HamburgerException(`Invalid topping ${topping}`)
        }
        this.toppings.push(topping);
    } catch (error) {
        console.log(`${error.name}: ${error.message}`);
    }
};

Hamburger.prototype.removeTopping = function (topping) {
    try {
        if (!topping) {
            HamburgerException('No property "topping" to remove');
        }
        this.toppings.forEach(item => {
            let index = this.toppings.indexOf(item);
            if (item === topping) {
                this.toppings.splice(index, 1);
            }
        });
    } catch (error) {
        console.log(`${error.name}: ${error.message}`);
    }
};

Hamburger.prototype.calculatePrice = function () {
    let price = 0;
    if (this.size === Hamburger.SIZE_SMALL) {
        price = 50;
    }
    if (this.size === Hamburger.SIZE_LARGE) {
        price = 100;
    }
    if (this.stuffing === Hamburger.STUFFING_CHEESE) {
        price += 10;
    }
    if (this.stuffing === Hamburger.STUFFING_SALAD) {
        price += 20;
    }
    if (this.stuffing === Hamburger.STUFFING_POTATO) {
        price += 15;
    }

    this.toppings.forEach(item => {
            if (item === Hamburger.TOPPING_MAYO) {
                price += 20
            }
            if (item === Hamburger.TOPPING_SPICE) {
                price += 15
            }
        }
    );
    return price;
};

Hamburger.prototype.calculateCalories = function (topping) {
    let calories = 0;
    if (this.size === Hamburger.SIZE_SMALL) {
        calories = 20;
    }
    if (this.size === Hamburger.SIZE_LARGE) {
        calories = 40;
    }
    if (this.stuffing === Hamburger.STUFFING_CHEESE) {
        calories += 20;
    }
    if (this.stuffing === Hamburger.STUFFING_SALAD) {
        calories += 5;
    }
    if (this.stuffing === Hamburger.STUFFING_POTATO) {
        calories += 10;
    }
    this.toppings.forEach(item => {
            if (item === Hamburger.TOPPING_MAYO) {
                calories += 5
            }
        }
    );
    return calories;
};

Hamburger.prototype.getToppings = function () {
    return this.toppings
};

Hamburger.prototype.getSize = function () {
    return this.size
};

Hamburger.prototype.getStuffing = function () {
    return this.stuffing
};

function HamburgerException(message) {
    var error = new Error;
    error.name = 'HamburgerException';
    error.message = message;
    throw error;
}

var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
hamburger.addTopping(Hamburger.TOPPING_MAYO);

console.log('The price is ', hamburger.calculatePrice());

console.log('Amount of calories is', hamburger.calculateCalories());

hamburger.addTopping(Hamburger.TOPPING_SPICE);
hamburger.addTopping(Hamburger.TOPPING_SPICE);

console.log('Is there two toppings? - ', hamburger.getToppings().length === 2);

console.log(hamburger.getSize() === Hamburger.SIZE_SMALL);

hamburger.removeTopping(Hamburger.TOPPING_SPICE);

console.log('Is there one toppings? - ', hamburger.getToppings().length === 1);

var ham2 = new Hamburger(Hamburger.TOPPINMAYO, Hamburger.STUFFING_SALAD);
