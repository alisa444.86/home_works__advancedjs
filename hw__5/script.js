/**

 Как только с сервера будет получена информация о персонажах какого-либо фильма,
 вывести эту информацию на экран под названием фильма.
 */

const filmsList = document.querySelector('.films');
const request = new XMLHttpRequest();
request.open('GET', 'https://swapi.co/api/films/');
request.send();

request.onload = function () {
    if (request.status >= 300) {
        console.log(`Error ${request.status}: ${request.statusText}`);
    } else {
        const response = JSON.parse(request.response);

        response.results.forEach(movie => {
            const movieItem = document.createElement('ul');
            const charactersList = document.createElement('ul');
            movieItem.classList.add('movie-item');
            const filmTitle = movie.title;
            const episodeId = movie.episode_id;
            const openingCrawl = movie.opening_crawl;
            const charactersLinks = movie.characters;

            movieItem.innerHTML = `
            <li><span class="bold">Title of Movie:</span> "${filmTitle}"</li>
            <li><span class="bold">Number of Episode:</span> ${episodeId}</li>
            <li><span class="bold">Opening Crawl:</span>                  
            "${openingCrawl}"</li>
            <li><span class="bold">Characters:</span></li>
           `;

            filmsList.append(movieItem);

            charactersLinks.forEach(link => {
                const character = document.createElement('li');
                const characterRequest = new XMLHttpRequest();

                characterRequest.open('GET', link);
                characterRequest.send();

                characterRequest.onload = function () {
                    if (characterRequest >= 300) {
                        console.log(`Error ${request.status}: ${request.statusText}`);
                    } else {
                        const response = JSON.parse(characterRequest.response);
                        character.textContent = response.name;
                        charactersList.append(character);
                    }
                };
            });

            movieItem.append(charactersList);
        });
    }
};

request.onerror = function () {
    alert("Request failed");
};
