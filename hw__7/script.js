/**

 Как только с сервера будет получена информация о персонажах какого-либо фильма,
 вывести эту информацию на экран под названием фильма.
 */

const filmsList = document.querySelector('.films');
const request = fetch('https://swapi.co/api/films/');

request.then(response => response.json())
    .then(result => {
        result.results.forEach(movie => {
            const movieItem = document.createElement('ul');
            const charactersList = document.createElement('ul');
            const filmTitle = movie.title;
            const episodeId = movie.episode_id;
            const openingCrawl = movie.opening_crawl;
            const charactersLinks = movie.characters;

            movieItem.classList.add('movie-item');
            movieItem.innerHTML = `
            <li><span class="bold">Title of Movie:</span> "${filmTitle}"</li>
            <li><span class="bold">Number of Episode:</span> ${episodeId}</li>
            <li><span class="bold">Opening Crawl:</span>                  
            "${openingCrawl}"</li>
            <li><span class="bold">Characters:</span></li>
           `;

            filmsList.append(movieItem);

            const characterRequests = charactersLinks.map(link => fetch(link));

            Promise.all(characterRequests)
                .then(responses => {
                    responses.forEach(response => {
                        response = response.json()
                            .then(
                                result => {
                                    const character = document.createElement('li');
                                    character.textContent = result.name;
                                    charactersList.append(character);
                                })
                    });
                    movieItem.append(charactersList);
                })
        })
    })
