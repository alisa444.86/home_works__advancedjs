import {deleteRequest, putRequest} from "./ajax-requests.js";

class Card {
    createCard(id, title, body, users) {
        this.card = document.createElement('div');
        this.users = users;
        this.card.id = id;
        this.card.classList.add('card');
        let userName, email;
        this.users.forEach(user => {
            if (user.id === id) {
                userName = user.name;
                email = user.email;
            }
        });

        this.card.innerHTML = this.getTemplate(userName, email, title, body);
        this.addCardBtnListeners();
        return this.card;
    }

    addCardBtnListeners() {
        this.cardEditBtn = this.card.querySelector('.edit');
        this.cardDeleteBtn = this.card.querySelector('.delete');

        this.cardEditBtn.addEventListener('click', (event) => {
            if (event.target.innerText === 'Edit') {
                this.editCard.call(this, event.target.parentNode);
            } else {
                this.saveCard.call(this, event.target.parentNode);
            }
        });
        this.cardDeleteBtn.addEventListener('click', (event) => {
            this.deleteCard.call(this, event.target.parentNode)
        });
    }

    targetCardFields(card) {
        this.cardTitle = card.querySelector('.card-title');
        this.cardText = card.querySelector('.card-body');
        this.inputTitle = card.querySelector('.card-input-title');
        this.inputText = card.querySelector('.card-input-text');
        this.editBtn = card.querySelector('.edit');
    }

    editCard(card) {
        this.targetCardFields(card);

        this.cardTitle.style.display = 'none';
        this.cardText.style.display = 'none';
        this.inputTitle.style.display = 'block';
        this.inputText.style.display = 'block';
        this.editBtn.innerText = 'Save';
    }

    saveCard(card) {
        this.targetCardFields(card);

        this.cardTitle.textContent = this.inputTitle.value;
        this.cardText.textContent = this.inputText.value;
        this.inputTitle.style.display = 'none';
        this.inputText.style.display = 'none';
        this.cardTitle.style.display = 'block';
        this.cardText.style.display = 'block';
        this.editBtn.innerText = 'Edit';

        let body = {
            userId: 1,
            id: card.id,
            title: this.cardTitle.textContent,
            body: this.cardText.textContent
        };

        body = JSON.stringify(body);
        putRequest(card.id, body);
    }

    deleteCard(card) {
        deleteRequest(card.id);
        card.remove();
    }

    getTemplate(name, email, title, body) {
        return `
            <p class="user-name">${name}</p>
            <p class="user-email">${email}</p>
            <h4 class="card-title">${title}</h4>
            <textarea rows="2" cols="50" class="card-input-title">${title}</textarea>
            <p class="card-body">${body}</p>
            <textarea rows="5" cols="50" class="card-input-text">${body}</textarea>
            <button class="delete">Delete</button>
            <button class="edit">Edit</button>
            `
    }

    renderCard(container, card) {
        container.insertAdjacentElement('afterbegin', this.card);
    }
}

export default Card;