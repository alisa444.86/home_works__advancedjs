

const btn = document.querySelector('.get-info');
const infoBox = document.querySelector('.info');
btn.addEventListener('click', getInfo);

async function getInfo() {
    const response = await fetch('https://api.ipify.org/?format=json');
    if (response.ok) {
        const {ip} = await response.json();
        const info = await fetch(
            `http://ip-api.com/json/${ip}?lang=ru&fields=continent,country,regionName,city,district`);
        const result = await info.json();
        console.log(result);

        infoBox.innerHTML = `
        <p>Континент: ${result.continent}</p>
        <p>Страна: ${result.country}</p>
        <p>Регион: ${result.regionName}</p>
        <p>Город: ${result.city}</p>
        <p>Район города: ${result.district}</p>
        `
    } else {
        alert("Ошибка HTTP: " + response.status);
    }
}
