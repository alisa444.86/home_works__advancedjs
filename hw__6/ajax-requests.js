async function getPosts() {
    try {
        const response = await fetch('https://jsonplaceholder.typicode.com/posts');
        let posts = await response.json();
        return posts;
    } catch (error) {
        alert('Server is not responding. Reload the window, please!');
    }
}

async function getUsers() {
    try {
        const response = await fetch('https://jsonplaceholder.typicode.com/users');
        let users = await response.json();
        return users
    } catch (error) {
        alert('Server is not responding. Reload the window, please!');
    }
}

function putRequest(postId, body) {
    fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`, {
        method: 'PUT',
        body: body,
        headers: {
            'Content-Type': 'application/json'
        }
    }).catch(err => console.log(err));
}

function deleteRequest(postId) {
    fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        }
    }).catch(err => console.log(err));
}

function postRequest(body) {
    fetch(`https://jsonplaceholder.typicode.com/posts`, {
        method: 'POST',
        body: body,
        headers: {
            'Content-Type': 'application/json; charset=UTF-8'
        },
    }).catch(err => console.log(err));
}

export {getPosts, getUsers, deleteRequest, postRequest, putRequest}