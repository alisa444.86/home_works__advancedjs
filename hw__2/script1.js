class Hamburger {
    constructor(size, stuffing) {
            if (!size) {
                HamburgerException('No given size');
            }
            if (!stuffing) {
                HamburgerException('No given stuffing');
            }
            if (size !== Hamburger.SIZE_LARGE && size !== Hamburger.SIZE_SMALL) {
                HamburgerException(`invalid size ${size}`);
            }
            if (stuffing !== Hamburger.STUFFING_CHEESE && stuffing !== Hamburger.STUFFING_POTATO &&
                stuffing !== Hamburger.STUFFING_SALAD) {
                HamburgerException(`invalid stuffing ${stuffing}`);
            }
            this.size = size;
            this.stuffing = stuffing;
            this.toppings = [];
    }

    static SIZE_SMALL = 'SIZE_SMALL';
    static STUFFING_CHEESE = 'STUFFING_CHEESE';
    static SIZE_LARGE = 'SIZE_LARGE';
    static STUFFING_SALAD = 'STUFFING_SALAD';
    static STUFFING_POTATO = 'STUFFING_POTATO';
    static TOPPING_MAYO = 'TOPPING_MAYO';
    static TOPPING_SPICE = 'TOPPING_SPICE';

    addTopping(topping) {
            this.toppings.forEach(item => {
                if (topping === item) {
                    HamburgerException(`Duplicate topping ${topping}`)
                }
            });
            if (topping !== Hamburger.TOPPING_MAYO && topping !== Hamburger.TOPPING_SPICE) {
                HamburgerException(`Invalid topping ${topping}`)
            }
            this.toppings.push(topping);
    }

    removeTopping(topping) {
            if (!topping) {
                HamburgerException('No property "topping" to remove');
            }
            this.toppings.forEach(item => {
                let index = this.toppings.indexOf(item);
                if (item === topping) {
                    this.toppings.splice(index, 1);
                }
            });
    }

    calculatePrice() {
        let price = 0;
        if (this.size === Hamburger.SIZE_SMALL) {
            price = 50;
        }
        if (this.size === Hamburger.SIZE_LARGE) {
            price = 100;
        }
        if (this.stuffing === Hamburger.STUFFING_CHEESE) {
            price += 10;
        }
        if (this.stuffing === Hamburger.STUFFING_SALAD) {
            price += 20;
        }
        if (this.stuffing === Hamburger.STUFFING_POTATO) {
            price += 15;
        }

        this.toppings.forEach(item => {
                if (item === Hamburger.TOPPING_MAYO) {
                    price += 20
                }
                if (item === Hamburger.TOPPING_SPICE) {
                    price += 15
                }
            }
        );
        return price;
    }

    calculateCalories(topping) {
        let calories = 0;
        if (this.size === Hamburger.SIZE_SMALL) {
            calories = 20;
        }
        if (this.size === Hamburger.SIZE_LARGE) {
            calories = 40;
        }
        if (this.stuffing === Hamburger.STUFFING_CHEESE) {
            calories += 20;
        }
        if (this.stuffing === Hamburger.STUFFING_SALAD) {
            calories += 5;
        }
        if (this.stuffing === Hamburger.STUFFING_POTATO) {
            calories += 10;
        }
        this.toppings.forEach(item => {
                if (item === Hamburger.TOPPING_MAYO) {
                    calories += 5
                }
            }
        );
        return calories;
    }

    getToppings() {
        return this.toppings
    }

    getSize() {
        return this.size
    }

    getStuffing() {
        return this.stuffing
    }
}

function HamburgerException(message) {
    let error = new Error;
    error.name = 'HamburgerException';
    error.message = message;
    throw error;
}

let newHamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);

try {
    newHamburger.addTopping(Hamburger.TOPPING_MAYO);
    newHamburger.addTopping(Hamburger.TOPPING_MAYO);
} catch (e) {
    console.log(`${e.name}: ${e.message}`);
}

try {
    console.log('The price is ', newHamburger.calculatePrice());
    console.log('Amount of calories is', newHamburger.calculateCalories());
} catch (e) {
    console.log(`${e.name}: ${e.message}`);
}

try {
    newHamburger.addTopping(Hamburger.TOPPING_SPICE);
    newHamburger.addTopping(Hamburger.TOPPING_SPICE);
} catch (e) {
    console.log(`${e.name}: ${e.message}`);
}

try {
    console.log('Is there two toppings? - ', newHamburger.getToppings().length === 2);
    console.log('Is the Hamburger size - small?' + newHamburger.getSize() === Hamburger.SIZE_SMALL);
    newHamburger.removeTopping(Hamburger.TOPPING_SPICE);
    console.log('Is there one toppings? - ', newHamburger.getToppings().length === 1);
} catch (e) {
    console.log(`${e.name}: ${e.message}`);
}

