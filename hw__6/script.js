import Card from './card.js';
import {getUsers, getPosts, postRequest} from "./ajax-requests.js";

class Twitter {
    constructor() {
        this.container = document.querySelector('.root');
        this.addPostBtn = document.querySelector('.add-post');
        this.newCard = new Card();
        this.getFieldsOfModal();
        getUsers().then(users => this.users = users)
            .then(getPosts).then(posts => this.posts = posts)
            .then(posts => {
                this.renderPosts();
            })
            .then(this.initEvents.bind(this));
    }

    getFieldsOfModal() {
        this.modalWindow = document.querySelector('.modal');
        this.modalForm = document.querySelector('.modal-form');
        this.inputTitle = this.modalForm.querySelector('.input-title');
        this.inputText = this.modalForm.querySelector('.input-body');
        this.submitBtn = this.modalForm.querySelector('.submit-btn');
        this.cancelBtn = this.modalForm.querySelector('.cancel');
    }

    renderPosts() {
        this.posts.forEach((post) => {
            const userId = post.userId;
            const postTitle = post.title;
            const postBody = post.body;
            this.card = this.newCard.createCard(userId, postTitle, postBody, this.users);
            this.newCard.renderCard(this.container, this.card);
        });
    }

    initEvents() {
        this.addPostBtn.addEventListener('click', this.modalOpen.bind(this));
        this.submitBtn.addEventListener('click', this.createNewPost.bind(this));
        this.cancelBtn.addEventListener('click', this.modalClose.bind(this));
    }

    createNewPost() {
        if (this.inputTitle.value && this.inputText.value) {
            this.newCard.createCard(this.users[0].id, this.inputTitle.value, this.inputText.value, this.users);
            let body = JSON.stringify({
                userId: 1,
                id: this.card.id,
                title: this.card.querySelector('.card-title').textContent,
                body: this.card.querySelector('.card-body').textContent
            });
            this.newCard.renderCard(this.container, this.card);
            this.modalClose();
            this.autoScroll();
            postRequest(body);
        }
    }

    autoScroll() {
        window.scrollTo(pageXOffset, 0);
    }

    modalOpen() {
        this.modalWindow.style.display = 'block';
    }

    modalClose() {
        this.modalWindow.style.display = 'none';
        this.inputTitle.value = '';
        this.inputText.value = '';
    }
}

window.onload = () => {
    let twitter = new Twitter()
};

