function Hamburger(size, stuffing) {
    try {
        if (!size) {
            HamburgerException('No given size');
        }
        if (!stuffing) {
            HamburgerException('No given stuffing');
        }
        if (size !== Hamburger.SIZE_LARGE && size !== Hamburger.SIZE_SMALL) {
            HamburgerException(`invalid size`);
        }
        if (stuffing !== Hamburger.STUFFING_CHEESE && stuffing !== Hamburger.STUFFING_POTATO &&
            stuffing !== Hamburger.STUFFING_SALAD) {
            HamburgerException(`invalid stuffing`);
        }

        this.size = size;
        this.stuffing = stuffing;
        this.toppings = [];

    } catch (error) {
        console.log(`${error.name}: ${error.message}`);
    }
}

Hamburger.SIZE_SMALL = {
    price: 50,
    calories: 20
};
Hamburger.SIZE_LARGE = {
        price: 100,
        calories: 40
    };
Hamburger.STUFFING_CHEESE = {
        price: 10,
        calories: 20
    };
Hamburger.STUFFING_SALAD = {
    price: 20,
    calories: 5
};
Hamburger.STUFFING_POTATO = {
    price: 15,
    calories: 10
};
Hamburger.TOPPING_MAYO = {
    price: 20,
    calories: 5
};
Hamburger.TOPPING_SPICE = {
    price: 15,
    calories: 0
};

Hamburger.prototype.addTopping = function (topping) {
    try {
        this.toppings.forEach(item => {
            if (topping === item) {
                HamburgerException(`Duplicate topping`)
            }
        });
        if (topping !== Hamburger.TOPPING_MAYO && topping !== Hamburger.TOPPING_SPICE) {
            HamburgerException(`Invalid topping`)
        }
        this.toppings.push(topping);
    } catch (error) {
        console.log(`${error.name}: ${error.message}`);
    }
};

Hamburger.prototype.removeTopping = function (topping) {
    try {
        if (!topping) {
            HamburgerException('No property "topping" to remove');
        }
        this.toppings.forEach(item => {
            let index = this.toppings.indexOf(item);
            if (item === topping) {
                this.toppings.splice(index, 1);
            }
        });
    } catch (error) {
        console.log(`${error.name}: ${error.message}`);
    }
};

Hamburger.prototype.calculatePrice = function () {

    var price = this.size.price + this.stuffing.price;
    this.toppings.forEach(item => {
        price += item.price;
    });
    return price;
};

Hamburger.prototype.calculateCalories = function () {

    var calories = this.size.calories + this.stuffing.calories;
    this.toppings.forEach(item => {
        calories += item.calories;
    });
    return calories
};

Hamburger.prototype.getToppings = function () {
    return this.toppings
};

Hamburger.prototype.getSize = function () {
    return this.size
};

Hamburger.prototype.getStuffing = function () {
    return this.stuffing
};

function HamburgerException(message) {
    var error = new Error;
    error.name = 'HamburgerException';
    error.message = message;
    throw error;
}

var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);

hamburger.addTopping(Hamburger.TOPPING_MAYO);

console.log('The price is ', hamburger.calculatePrice());

console.log('Amount of calories is', hamburger.calculateCalories());

hamburger.addTopping(Hamburger.TOPPING_SPICE);
hamburger.addTopping(Hamburger.TOPPING_SPICE);

console.log('Is there two toppings? - ', hamburger.getToppings().length === 2);

console.log('Is the size of hamburger - small? - ', hamburger.getSize() === Hamburger.SIZE_SMALL);

hamburger.removeTopping(Hamburger.TOPPING_SPICE);

console.log('Is there one toppings? - ', hamburger.getToppings().length === 1);

hamburger.addTopping(Hamburger.SIZE_SMALL);

var ham2 = new Hamburger();


