class Game {
    constructor(timeout) {
        this.target = null;
        this.counterComp = 0;
        this.counterPlayer = 0;
        this.timeout = timeout || 1000;
        this.cells = document.querySelectorAll('.cell');
        this.cellsArray = Array.from(this.cells);
        this.table = document.querySelector('table');

        this.interval = setInterval(this.colorCell.bind(this), this.timeout);
        this.table.addEventListener('click', this.listenClick.bind(this));
    }

    listenClick(event) {
        if (event.target.classList.contains('cell') && event.target.classList.contains('blue')) {
            event.target.classList.add('green');
            this.counterPlayer++;
            if (this.counterPlayer === this.cells.length / 2) {
                setTimeout(() => {
                    alert(`Congratulations!
                    You won`);
                }, 0);
                this.gameOver();
                return
            }
        }
    }

    colorCell() {
        if (this.target && this.target.classList.contains('blue') && !this.target.classList.contains('green') && !this.target.classList.contains('red')) {
            this.target.classList.remove('blue');
            this.target.classList.add('red');
            this.counterComp++;
            if (this.counterComp === this.cells.length / 2) {
                //hack for chrome
                setTimeout(() => {
                    alert(`Game Over!
                    Computer won`);
                }, 0);
                this.gameOver();
                return
            }
        }

        const randomIndex = Math.floor(Math.random() * this.cellsArray.length);
        if (!this.cellsArray[randomIndex].classList.contains('blue') &&
            !this.cellsArray[randomIndex].classList.contains('green') &&
            !this.cellsArray[randomIndex].classList.contains('red'))

        {
            this.cells[randomIndex].classList.add('blue');
            this.cellsArray.splice(randomIndex, 1);
            this.target = this.cells[randomIndex];
        }

    }

    gameOver() {
        clearInterval(this.interval);
    }

    deleteGame() {
        this.target = null;
        this.counterComp = 0;
        this.counterPlayer = 0;
        this.cells.forEach(item => {
            item.classList = 'cell';
        });
        this.table.removeEventListener('click', this.listenClick);
        this.gameOver();
    }
}

class lightLevel extends Game {
    constructor(timeout) {
        super(1500);
    }
}

class midLevel extends Game {
    constructor(timeout) {
        super(1000);
    }
}

class highLevel extends Game {
    constructor(timeout) {
        super(500);
    }
}

let game;
const startBtn = document.querySelector('.start-game');

startBtn.addEventListener('click', () => {
    if (game) {
        game.deleteGame.call(game);
    }
    let level = document.querySelector('input[name="level"]:checked').value;

    switch (level) {
        case 'light':
            game = new lightLevel();
            break;
        case 'middle':
            game = new midLevel();
            break;
        case 'high':
            game = new highLevel();
    }
});