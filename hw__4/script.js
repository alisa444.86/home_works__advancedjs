/**
 * Существующие карточки можно перетягивать вверх и вниз, меняя их порядок в колонке.

 В верхней части колонки должна быть кнопка для сортировки карточек в колонке по алфавиту.
 После сортировки карточки можно свободно перетягивать и менять местами.
 */
const wrapper = document.querySelector('.wrapper');

class Column {

    constructor() {
        this.btnAddColumn = document.querySelector('.add-column');
        this.column = document.createElement('div');
        this.cards = [];
        this.render(wrapper);
        this.addCardBtn = this.column.querySelector('.addBtn');
        this.sortCardBtn = this.column.querySelector('.sortBtn');
        this.deleteColumnBtn = this.column.querySelector('.delete-column');
        this.cardsContainer = this.column.querySelector('.cards-container');
        this.initEvents();
    }

    render(container) {
        this.column.innerHTML = (`<header class="header">
<button class="sortBtn">Sort</button>
<p class="column-title">To Do List</p>
<button class="delete-column">X</button>
</header>
<div class="cards-container"></div>
<div class="btns-container">
<button class="addBtn">+</button>
</div>
`);
        this.column.classList.add('column');
        container.append(this.column);
    }

    initEvents() {
        this.btnAddColumn.addEventListener('click', this.newColumn);
        this.addCardBtn.addEventListener('click', this.createCard.bind(this));
        this.sortCardBtn.addEventListener('click', this.sortCards.bind(this));
        this.deleteColumnBtn.addEventListener('click', this.deleteColumn.bind(this));
        this.dragAndDropListeners();
    }

    newColumn() {
        let column = new Column;
    };

    deleteColumn() {
        this.column.remove()
    }

    createCard() {
        if (!document.querySelector('input')) {
            this.input = document.createElement('input');
            this.input.type = "text";
            this.input.placeholder = "Enter a header for this card";
            this.input.classList.add('input');
            this.cardsContainer.append(this.input);
            this.input.addEventListener('blur', this.renderCard.bind(this));
        } else {
            return false
        }
    }

    renderCard() {
        const card = document.createElement('div');
        if (this.input.value) {
            const cardText = this.input.value;
            card.classList.add('card');
            card.innerText = cardText;
            card.draggable = true;
            card.addEventListener("dragstart", this.setId.bind(this));
            this.removeInput();
            this.cardsContainer.append(card);
            this.cards.push(card);
        } else {
            this.removeInput();
        }

    }

    removeInput() {
        if (this.input) {
            this.input.remove();
        }
    }

    sortCards() {
        this.removeInput();
        this.cards.sort(function (a, b) {
            if (a.textContent < b.textContent) return -1;
            if (a.textContent > b.textContent) return 1;
            return 0
        });
        this.deleteAllCards();
        this.appendSortedCards();
    }

    appendSortedCards() {
        this.cards.forEach(card => {
            this.cardsContainer.append(card);
        })
    }

    deleteAllCards() {
        this.cards.forEach(
            item => {
                item.remove();
            }
        );
    }

    drop(ev) {
        ev.preventDefault();

        const id = ev.dataTransfer.getData("id");
        const dragElement = document.getElementById(id);

        if (ev.target.draggable) {
            const targetCoords = ev.target.getBoundingClientRect();
            const centerOfTarget = targetCoords.y + targetCoords.height / 2;

            if (ev.clientY < centerOfTarget) {
                ev.target.before(dragElement)
            } else {
                ev.target.after(dragElement);
            }
        }
        dragElement.removeAttribute('id');
    }

    findSiblings(el) {
        return [...el.parentElement.children];
    }

    allowDrop(ev) {
        ev.preventDefault();
    }

    setId(ev) {
        const elements = this.findSiblings(ev.target);
        const id = elements.indexOf(ev.target);
        const classes = ev.target.className.split(" ").join("-");
        ev.target.id = `${classes}-${id}`;
        ev.dataTransfer.setData('id', ev.target.id);
    }

    dragAndDropListeners() {
        this.column.addEventListener("drop", this.drop.bind(this));
        this.column.addEventListener("dragover", this.allowDrop);
    }
}

let column = new Column();



